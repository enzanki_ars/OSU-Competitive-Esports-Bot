from osu_competitive_esports_bot.main import DISCORD_BOT
from osu_competitive_esports_bot.utils.email_check import valid_email
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command(name='verify-email')
@commands.check(valid_email)
async def verify_email(ctx, email):
    import requests

    from osu_competitive_esports_bot.main import AUTH0_CLIENT_ID, AUTH0_CLIENT_SECRET, DISCORD_JOIN_ROLES, delete_message, email_memory
    from osu_competitive_esports_bot.utils.osu_check import is_user_osu

    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !verify-email command.")

    if await is_user_osu(ctx):
        await ctx.message.author.send(
            ctx.message.author.mention + ' - ' +
            'It looks like you already have the OSU role. If you are not expecting this, please summon @Developer in '
            'the OSUBGC Discord group to alert them of the issue.')
    else:
        email_memory[ctx.message.author.name] = email
        response = requests.post("https://osu-esports-discord-bot.auth0.com/passwordless/start", json={
            "client_id": AUTH0_CLIENT_ID,
            "client_secret": AUTH0_CLIENT_SECRET,
            "connection": "email",
            "email": email,
            "send": "code"
        })
        if response.status_code == 200:
            await ctx.message.author.send(
                ctx.message.author.mention + ' - ' +
                'An authorization code has been sent to your OSU email. This may take a minute to send. '
                'Once you have the code, reply here using `!verify-code <code>`, replacing the `<code>` with the '
                'verification code that was just emailed to you.')
        else:
            await ctx.message.author.send(
                ctx.message.author.mention + ' - ' +
                'An error has occurred. Please summon @Developer in the OSUBGC Discord group to '
                'alert them of the issue.')

    await delete_message(ctx)
    logger.info("!verify-email command complete.")


@verify_email.error
async def verify_email_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CheckFailure):
        await ctx.message.author.send('A valid email was not provided.  Please ensure that the email contains `@osu.edu` or `@buckeyemail.osu.edu`')
    elif isinstance(error, commands.CommandError):
        await ctx.message.author.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
