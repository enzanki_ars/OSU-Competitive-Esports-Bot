from osu_competitive_esports_bot.main import DISCORD_BOT
from osu_competitive_esports_bot.utils.email_check import valid_code
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command(name='verify-code')
@commands.check(valid_code)
async def verify_code(ctx, code):
    import time

    import requests

    import discord

    from osu_competitive_esports_bot.main import AUTH0_CLIENT_ID, \
        AUTH0_CLIENT_SECRET, GOOGLE_SERVICE_SHEETS, SPREADSHEET_ID, SPREADSHEET_RANGE, \
        delete_message, email_memory, DISCORD_VERIFICATION_ROLES, DISCORD_GUEST_ROLES, DISCORD_GUEST_LOGS

    from osu_competitive_esports_bot.utils.osu_check import is_user_osu


    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !verify-code command.")

    if await is_user_osu(ctx):
        await ctx.message.author.send(
            ctx.message.author.mention + ' - ' +
            'It looks like you already have the OSU role. If you are not expecting this, please summon @Developer in '
            'the OSUBGC Discord group to alert them of the issue.')
    else:
        response = requests.post("https://osu-esports-discord-bot.auth0.com/oauth/token", json={
            "grant_type": "password",
            "username": email_memory[ctx.message.author.name],
            "password": code,
            "audience": "https://osu-esports-discord-bot.auth0.com/api/v2/",
            "scope": "openid email profile",
            "client_id": AUTH0_CLIENT_ID,
            "client_secret": AUTH0_CLIENT_SECRET
        })

        role_delivered = False

        if response.status_code == 200:

            for guild in ctx.bot.guilds:
                member = discord.utils.find(lambda m: m.id == ctx.message.author.id, guild.members)
                if member:
                    for role in DISCORD_VERIFICATION_ROLES:
                        role_to_add = discord.utils.find(lambda r: r.name == role, guild.roles)
                        if role_to_add:
                            print('delivered role')
                            role_delivered = True
                            await member.add_roles(role_to_add)
                    for guest_role in DISCORD_GUEST_ROLES:
                        role_to_remove = discord.utils.find(lambda r: r.name == guest_role, guild.roles)
                        if role_to_remove:
                            await member.remove_roles(role_to_remove)
                    for channel in DISCORD_GUEST_LOGS:
                        print('sending guest')
                        guest_log = discord.utils.find(lambda c: c.name == channel, guild.channels)
                        if guest_log:
                            await guest_log.send('Verified @' + member.name + '#' + member.discriminator)
            
            if role_delivered:
                print('alerting user')
                await ctx.message.author.send(
                    ctx.message.author.mention + ' - ' +
                    'Congratulations.  You have been verified.  You should now have the OSU role.')
                print('sending to sheets')
                GOOGLE_SERVICE_SHEETS.spreadsheets().values().append(
                    spreadsheetId=SPREADSHEET_ID,
                    range=SPREADSHEET_RANGE,
                    valueInputOption='USER_ENTERED',
                    body={
                        'range': SPREADSHEET_RANGE,
                        'majorDimension': 'ROWS',
                        'values': [[time.strftime("%x %X"),
                                    email_memory[ctx.message.author.name],
                                    ctx.message.author.name]]
                    }
                ).execute()
            else:
                print('failure')
                await ctx.message.author.send(
                    ctx.message.author.mention + ' - ' +
                    'An error has occurred. Please summon @Developer in the OSU Competitive Esports group to '
                    'alert them of the issue'
                )
        else:
            print('failure')
            await ctx.message.author.send(
                ctx.message.author.mention + ' - ' +
                'An error has occurred. Please summon @Developer in the OSU Competitive Esports group to '
                'alert them of the issue'
            )

    await delete_message(ctx)
    logger.info("!verify-email command complete.")


@verify_code.error
async def verify_code_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CheckFailure):
        await ctx.message.author.send('A valid code was not provided.  The code should be digits only and is only valid for about 30 minutes.')
    elif isinstance(error, commands.CommandError):
        await ctx.message.author.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
