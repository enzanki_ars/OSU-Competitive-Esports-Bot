from osu_competitive_esports_bot.utils.role_list import send_full_role_list
from osu_competitive_esports_bot.utils.admin_check import admin_check
from osu_competitive_esports_bot.main import DISCORD_BOT
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command()
@commands.check(admin_check)
async def debug(ctx):
    from osu_competitive_esports_bot.main import delete_message

    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !debug command.")

    await send_full_role_list(ctx.bot, ctx.message)
    await delete_message(ctx)
    logger.info("!debug command complete.")


@debug.error
async def debug_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CheckFailure):
        await ctx.message.author.send('You do not have permission to use this command.')
    elif isinstance(error, commands.CommandError):
        await ctx.message.author.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
