from osu_competitive_esports_bot.main import DISCORD_BOT
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command(name='guest')
async def guest_role(ctx):
    from osu_competitive_esports_bot.main import delete_message, DISCORD_GUEST_ROLES, DISCORD_GUEST_LOGS
    from osu_competitive_esports_bot.utils.osu_check import is_user_osu

    print(DISCORD_GUEST_ROLES)

    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !guest command.")

    if await is_user_osu(ctx):
        await ctx.message.author.send(
            ctx.message.author.mention + ' - ' +
            'It looks like you already have the OSU role. If you are not expecting this, please summon @Developer in '
            'the OSU Competitive Esports group to alert them of the issue or email '
            '`incoming+osu-competetive-esports/OSU-Competitive-Esports-Discord-Bot@incoming.gitlab.com`')
    else:
        for guild in ctx.bot.guilds:
            member = guild.get_member(ctx.author.id)
            if member is not None:
                for role in DISCORD_GUEST_ROLES:
                    print(role)
                    for guild_role in guild.roles:
                        if guild_role.name == role:
                            await member.add_roles(guild_role)
            for channel in guild.channels:
                if channel.name in DISCORD_GUEST_LOGS:
                    await channel.send('Gave guest role to @' + member.name + '#' + member.discriminator)
        await ctx.message.author.send(
            ctx.message.author.mention + ' - ' +
            'You now have the guest role.  If you are an OSU student/staff/faculty, please use the `!verify-email` '
            'command instead.  See `!help` for mor information ofn the `!verify-email` command.')

    await delete_message(ctx)
    logger.info("!guest command complete.")


@guest_role.error
async def guest_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CommandError):
        await ctx.message.author.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
