from osu_competitive_esports_bot.main import DISCORD_BOT
from discord.ext import commands

import logging

logger = logging.getLogger(__name__)

@commands.command()
async def join(ctx, group):
    from osu_competitive_esports_bot.main import delete_message, DISCORD_JOIN_ROLES
    from osu_competitive_esports_bot.utils.osu_check import is_user_osu

    logger.info(ctx.message.author.name + ": " + ctx.message.content)
    logger.info("Parsing !join command.")

    if not await is_user_osu(ctx):
        await ctx.send(
            ctx.message.author.mention + ' - ' +
            'It looks like you are not an OSU student.  Therefore, you are unable to join a '
            'league or committee. If you are an OSU student, use the `!verify-email` to verify '
            'that you are a student.')
    else:
        group_list = group.split(',')
        
        groups_added = []
        groups_removed = []
        groups_not_found = []

        member_to_edit = None

        
        for role in group_list:
            role = role.strip().lower()
            if role in DISCORD_JOIN_ROLES:
                for guild in ctx.bot.guilds:
                    member_to_edit = guild.get_member(ctx.author.id)
                    if member_to_edit is not None:
                        for guild_role in guild.roles:
                            if guild_role.name.lower() == role:
                                if guild_role not in member_to_edit.roles:
                                    groups_added.append(guild_role)
                                else:
                                    groups_removed.append(guild_role)
            else:
                groups_not_found.append(role)
        
        message_to_send = ctx.author.mention + '\n'

        if len(groups_added) != 0:
            await member_to_edit.add_roles(*groups_added)
            message_to_send += ':white_check_mark: Successfully added `' + \
                                ', '.join([role.name for role in groups_added]) + '` to your list of groups.\n'

        if len(groups_removed) != 0:
            await member_to_edit.remove_roles(*groups_removed)
            message_to_send += ':wastebasket: Successfully removed `' + \
                                ', '.join([role.name for role in groups_removed]) + '` from your list of groups.\n'

        if len(groups_not_found) != 0:
            message_to_send += ':warning: `' + ', '.join(groups_not_found) + '` was not in the list of groups.\n'

        await ctx.send(message_to_send)

    await delete_message(ctx)
    logger.info("!join command complete.")


@join.error
async def join_error(ctx, error):
    logger.warning(error)
    if isinstance(error, commands.CommandError):
        await ctx.send('Something happened and I was unable to complete this request...')

    from osu_competitive_esports_bot.main import delete_message
    await delete_message(ctx)
