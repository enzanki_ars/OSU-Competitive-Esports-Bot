async def send_allowed_role_list(discord_client, respond_to, prefix=None):
    from osu_competitive_esports_bot.main import settings

    if not prefix:
        await respond_to.send('Allowed roles are:\n')
    else:
        await respond_to.send(prefix + ' - Allowed roles are:\n')
    role_list = '```'
    for guild in discord_client.guilds:
        role_list += guild.name + ':\n'
        for role in guild.roles:
            if 'game_notification_roles' in settings['discord'] and \
                    role.name.lower() in settings['discord']['game_notification_roles']:
                role_list += '\t - ' + role.name + '\n'
    role_list += '```'
    await respond_to.send(role_list)


async def send_full_role_list(discord_client, message):
    from osu_competitive_esports_bot.main import settings, DISCORD_VERIFICATION_ROLES, DISCORD_ADMIN_ROLES

    role_list = 'Roles:\n'

    role_list += '```\n'
    role_list += 'Key:\n'
    role_list += 'Role Type,\tUser Count,\tName'
    role_list += '```\n'

    await message.author.send(role_list)
    role_list = ''

    for guild in discord_client.guilds:
        role_list += guild.name + ':\n'
        print(len(guild.roles))
        for chunk in [guild.roles[x:x + 50] for x in range(0, len(guild.roles), 50)]:
            role_list += '```\n'
            for role in chunk:
                if 'game_notification_roles' in settings['discord'] and \
                        role.name.lower() in settings['discord']['game_notification_roles']:
                    role_list += 'Notification,\t' + str(len(role.members)) + ',\t' + role.name + '\n'
                elif role.name in DISCORD_VERIFICATION_ROLES:
                    role_list += 'Verification,\t' + str(len(role.members)) + ',\t' + role.name + '\n'
                elif role.name in DISCORD_ADMIN_ROLES:
                    role_list += 'Admin,\t' + str(len(role.members)) + ',\t' + role.name + '\n'
                else:
                    role_list += 'None,\t' + str(len(role.members)) + ',\t' + role.name + '\n'
            role_list += '```'
            await message.author.send(role_list)
            role_list = ''
